import os
import random
import sys

sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))

from _timer import Timer

from eth.point import Point, PointCloud


N = 10
M = 100000

cloud_raw = [
    Point( random.random(), random.random() )
    for _ in range(M)
]
cloud = PointCloud(cloud_raw)

print("--- PointCloud.closest ---")

def closest(point, cloud):
    return min(cloud, key=lambda p: (point.x - p.x)**2 + (point.y - p.y)**2)

p = Point(random.random(), random.random())
with Timer() as t1:
    for i in range(N):
        closest(p, cloud_raw)
print("Python version (1): {:.3f}ms".format(t1.total()*1000/N))

try:
    with Timer() as t1:
        for i in range(N):
            cloud.closest(p)
    print("Cython version:     {:.3f}ms".format(t1.total()*1000/N))
except NotImplementedError:
    pass