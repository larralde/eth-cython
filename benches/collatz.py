import os
import sys

sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))

from _timer import Timer

try:
    from eth.collatz import collatz as collatz_pyx
except ImportError:
    collatz_pyx = None

N = 10000

print("--- Collatz ---")

def collatz_py(n):
    i = 0
    while n > 1:
        n = n*3 + 1 if n%2 else n//2
        i += 1
    return i

with Timer() as t1:
    for x in range(N):
        collatz_py(100000)
print("Python version: {:.3f}µs".format(t1.total()*1e6/N))

if collatz_pyx is not None:

    with Timer() as t3:
        for x in range(N):
            collatz_pyx(100000)
    print("Cython version: {:.3f}µs".format(t3.total()*1e6/N))