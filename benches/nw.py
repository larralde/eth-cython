import array
import os
import sys
import numpy

sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))

from _timer import Timer

try:
    from eth.nw import needleman_wunsch as needleman_wunsch_pyx
except ImportError:
    needleman_wunsch_pyx = None


seq1 = "MGLGTLYFLVKGMGVSDPDAKKFYAITTLVPAIAFTMYLSMLLGYGLTMVPFGGEQNPIYWARYADWLFTTPLLLLDLALLVDADQGTILALVGADGIMIGTGLVGALTKVYSYRFVWWAISTAAMLYILYVLFFGFTSKAESMRPEVASTFKVLRNVTVVLWSAYPVVWLIGSEGAGIVPLNIETLLFMVLDVSAKVGFGLILLRSRAIFGEAEAPEPSAGDGAAATSD"
seq2 = "MLIGTFYFIARGWGVTDKKAREYYAITILVPGIASAAYLSMFFGIGLTTVEVAGMAEPLEIYYARYADWLFTTPLLLLDLALLANADRTTIGTLIGVDALMIVTGLIGALSHTPLARYTWWLFSTIAFLFVLYYLLTVLRSAAAELSEDVQTTFNTLTALVAVLWTAYPILWIIGTEGAGVVGLGVETLAFMVLDVTAKVGFGFVLLRSRAILGETEAPEPSAGAEAQAAD"


def _score(x, y):
    return 1 if x == y else -1

def needleman_wunsch_py(seq1, seq2):
    a = array.array("q", (len(seq1)+1)*(len(seq2)+1)*[0])
    m = memoryview(a).cast("B").cast("q", shape=[len(seq1)+1, len(seq2)+1])

    for i in range(len(seq1)+1):
        m[i, 0] = -i
    for j in range(len(seq2)+1):
        m[0, j] = -j

    for i in range(1, len(seq1)+1):
        for j in range(1, len(seq2)+1):
            x1 = m[i-1, j-1] + _score(seq1[i-1], seq2[j-1])
            x2 = m[i-1, j] - 2
            x3 = m[i, j-1] - 2
            m[i, j] = max(x1, x2, x3)

    ali1 = []
    ali2 = []
    i = len(seq1)
    j = len(seq2)

    while i > 0 or j > 0:
        if i > 0 and j > 0 and m[i,j] == m[i-1, j-1] + _score(seq1[i-1], seq2[j-1]):
            ali1.append(seq1[i-1])
            ali2.append(seq2[j-1])
            i -= 1
            j -= 1
        elif i > 0 and m[i,j] == m[i-1, j] - 2:
            ali1.append(seq1[i-1])
            ali2.append("-")
            i -= 1
        else:
            ali1.append("-")
            ali2.append(seq2[j-1])
            j -= 1

    return "".join(reversed(ali1)), "".join(reversed(ali2))

N = 100

print("--- Needleman/Wunsch ---")

with Timer() as t1:
    for x in range(N):
        needleman_wunsch_py(seq1, seq2)
print("Python version: {:.3f}ms".format(t1.total()*1e3/N))

try:
    with Timer() as t3:
        for x in range(N):
            needleman_wunsch_pyx(seq1, seq2)
    print("Cython version: {:.3f}ms".format(t3.total()*1e3/N))
except NotImplementedError:
    pass