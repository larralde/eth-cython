import os
import sys

sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))

from _fasta import parse
from _timer import Timer

try:
    from eth.seq import gc_percent as gc_percent_pyx
except ImportError:
    gc_percent_pyx = None

try:
    from eth.seq import revcomp as revcomp_pyx
except ImportError:
    revcomp_pyx = None



data = next(parse("data/411476.SAMN00627058.fna")).seq
N = 10

if gc_percent_pyx is not None:

    print("--- GC% ---")

    def gc_percent_py(sequence):
        gc = 0
        length = 0
        for letter in sequence:
            if letter == "G" or letter == "C":
                gc += 1
            if letter != "N":
                length += 1
        return gc / length

    with Timer() as t1:
        for i in range(N):
            gc_percent_py(data)
    print("Python version: {:.3f}ms".format(t1.total()*1000/N))

    with Timer() as t2:
        for i in range(N):
            gc_percent_pyx(data)
    print("Cython version: {:.3f}ms".format(t2.total()*1000/N))


if revcomp_pyx is not None:

    print("--- Reverse Complement ---")

    def revcomp_py(sequence):
        rev = ""
        for letter in reversed(sequence):
            if letter == "G":
                rev += "C"
            elif letter == "C":
                rev += "G"
            elif letter == "A":
                rev += "T"
            elif letter == "T":
                rev += "A"
            else:
                rev += "N"
        return rev

    def revcomp_py2(sequence):
        rev = []
        for letter in reversed(sequence):
            if letter == "G":
                rev.append("C")
            elif letter == "C":
                rev.append("G")
            elif letter == "A":
                rev.append("T")
            elif letter == "T":
                rev.append("A")
            else:
                rev.append("N")
        return "".join(rev)

    with Timer() as t1:
        for i in range(N):
            revcomp_py(data)
    print("Python version (1): {:.3f}ms".format(t1.total()*1000/N))

    with Timer() as t1:
        for i in range(N):
            revcomp_py2(data)
    print("Python version (2): {:.3f}ms".format(t1.total()*1000/N))

    with Timer() as t1:
        for i in range(N):
            revcomp_pyx(data)
    print("Cython version:     {:.3f}ms".format(t1.total()*1000/N))