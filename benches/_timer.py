import time
import typing
from typing import Optional

class Timer(typing.ContextManager["Timer"]):
    start: Optional[float]
    end: Optional[float]

    def __init__(self) -> None:
        self.start: Optional[float] = None
        self.end: Optional[float] = None

    def __enter__(self) -> "Timer":
        self.end = None
        self.start = time.time()
        return self

    def __exit__(self, exc_value, exc_ty, tb) -> bool:
        self.end = time.time()
        return False

    def total(self) -> float:
        if self.end is None:
            raise RuntimeError("Timer has not stopped")
        assert self.start is not None
        return self.end - self.start