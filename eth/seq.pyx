# cython: language_level=3 

from cpython.unicode cimport PyUnicode_ReadChar, PyUnicode_New, PyUnicode_WriteChar

def gc_percent(sequence):
    cdef Py_UCS4 letter
    cdef size_t  i
    cdef size_t  gc     = 0
    cdef size_t  length = 0
    
    for i in range(len(sequence)):
        letter = PyUnicode_ReadChar(sequence, i)
        if letter == "G" or letter == "C":
            gc += 1
        if letter != "N":
            length += 1

    return float(gc) / length



def revcomp(sequence):
    cdef size_t  i
    cdef Py_UCS4 old_char
    cdef Py_UCS4 new_char
    cdef str     rc       = PyUnicode_New(len(sequence), 127)

    for i, j in enumerate(reversed(range(len(sequence)))):
        old_char = PyUnicode_ReadChar(sequence, i)
        if old_char == "A":
            new_char = "T"
        elif old_char == "T":
            new_char = "A"
        elif old_char == "C":
            new_char = "G"
        elif old_char == "G":
            new_char = "C"
        else:
            new_char = "N"
        PyUnicode_WriteChar(rc, j, new_char)
    
    return rc