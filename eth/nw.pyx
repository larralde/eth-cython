import array

cpdef tuple needleman_wunsch(str seq1, str seq2):
    cdef object    a = array.array("i", (len(seq1)+1)*(len(seq2)+1)*[0])
    cdef int[:, :] m = memoryview(a).cast("B").cast("i", shape=[len(seq1)+1, len(seq2)+1])

    # --- Matrix filling ---

    # --- Backtracking ---

    raise NotImplementedError