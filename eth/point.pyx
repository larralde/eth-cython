# cython: language_level=3

import array
import math
from libc.math cimport INFINITY

cdef class Point:
    cdef readonly float x
    cdef readonly float y

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def r(self):
        return math.sqrt(self.x**2 + self.y**2)

    def phi(self):
        return math.atan2(self.y, self.x)

cdef class PointCloudSlow:
    cdef list points

    def __init__(self, points):
        self.points = list(points)

    def __contains__(self, point):
        return point in self.points

    cpdef Point closest(self, Point point):
        cdef Point p
        cdef Point best_p
        
        cdef float dx
        cdef float dy
        cdef float d
        cdef float best_d = INFINITY

        for p in self.points:
            dx = p.x - point.x
            dy = p.y - point.y
            d = dx*dx + dy*dy
            if d < best_d:
                best_d = d
                best_p = p

        return best_p
    

cdef class PointCloud:
    cdef object x
    cdef object y

    def __init__(self, points):
        self.x = array.array('f')
        self.y = array.array('f')
        for point in points:
            self.x.append(point.x)
            self.y.append(point.y)

    def __contains__(self, Point point):
        cdef Py_ssize_t i
        cdef float[:] x = self.x
        cdef float[:] y = self.y
        for i in range(len(self.x)):
            if point.x == x[i] and point.y == y[i]:
                return True
        return False

    def closest(self, Point point):
        cdef float      dx
        cdef float      dy
        cdef size_t     i
        cdef float      d
        cdef size_t     best_i = 0
        cdef float      best_d = math.inf
        cdef float[:]   x      = self.x
        cdef float[:]   y      = self.y
        cdef float      px     = point.x
        cdef float      py     = point.y

        for i in range(x.shape[0]):
            dx = x[i] - px
            dy = y[i] - py
            d = dx*dx + dy*dy
            if d < best_d:
                best_d = d
                best_i = i

        return Point(x[best_i], y[best_i])